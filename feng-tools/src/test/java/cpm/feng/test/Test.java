package cpm.feng.test;

import com.feng.collection.CollectUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

import static java.util.stream.Collectors.*;

public class Test {

    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException, InstantiationException {
        List<User> list = Arrays.asList(new User("feng",22),
                new User("feng",19),
                new User("feng1",20),
                new User("feng2",21));
        System.out.println(CollectUtils.getList(list,User::getName));
        System.out.println(CollectUtils.getSet(list,User::getName));
        TreeSet<Integer> valCollection = CollectUtils.getCollection(list, User::getAge, TreeSet::new);
        System.out.println(valCollection);

        System.out.println(CollectUtils.toMap(list,User::getName,User::getAge));
        System.out.println(CollectUtils.toLinkedMap(list,User::getName,User::getAge));
        System.out.println(CollectUtils.toMap(list,User::getName,User::getAge, ConcurrentHashMap::new));

//        testSetTargetProperty();
//        testDistinctByKey();

//        testGetFlatList();

//        testGroupByKey();
//        testSum();
        testMinOrMax();
    }

    private static void testMinOrMax() {
        List<User> list = Arrays.asList(
                new User("feng",22, null,1.75,1L),
                new User("feng",19, null,1.25,2L),
                new User("feng1",20, new BigDecimal("200.78"),1.51,3L),
                new User("feng2",21, new BigDecimal("100.22"),1.52,4L));
        System.out.println(CollectUtils.min(list,User::getAge).getAge());
        System.out.println(CollectUtils.min(list,User::getMoney).getMoney());
        System.out.println(CollectUtils.max(list,User::getAge).getAge());
        System.out.println(CollectUtils.max(list,User::getMoney).getMoney());
    }

    private static void testSum() {
        List<User> list = Arrays.asList(
                new User("feng",22, new BigDecimal("500.98"),1.75,1L),
                new User("feng",20, new BigDecimal("500.02"),1.25,2L),
                new User("feng1",20, new BigDecimal("100.01"),1.51,3L),
                new User("feng2",21, new BigDecimal("100.02"),1.52,4L));

        System.out.println(CollectUtils.sumForInt(list,User::getAge));
        System.out.println(CollectUtils.sumForBigDecimal(list,User::getMoney));
        System.out.println(CollectUtils.sumForDouble(list,User::getHeight));
        System.out.println(CollectUtils.sumForLong(list,User::getLl));
    }


    private static void testGroupByKey() {
        List<Product> products = new ArrayList<>();
        Product p = new Product();
        p.setProductId("1");
        p.setShopId("1");
        products.add(p);
        p = new Product();
        p.setProductId("3");
        p.setShopId("2");
        products.add(p);
        p = new Product();
        p.setProductId("2");
        p.setShopId("1");
        products.add(p);
        p = new Product();
        p.setProductId("4");
        p.setShopId("2");
        products.add(p);
        Map<String, List<Product>> map = CollectUtils.groupByKey(products, Product::getShopId);
        map.keySet().forEach(shopId -> {
            System.out.println(shopId+"------");
            List<Product> pros = map.get(shopId);
            for(Product pro : pros){
                System.out.println("--------"+pro.getProductId());
            }
        });
        products = new ArrayList<>();
        LinkedHashMap<String, List<Product>> mm = CollectUtils.groupByKey(products, Product::getShopId, () -> new LinkedHashMap<>(), toList());
        mm.keySet().forEach(shopId -> {
            System.out.println(shopId+"------");
            List<Product> pros = map.get(shopId);
            for(Product pro : pros){
                System.out.println("--------"+pro.getProductId());
            }
        });
    }


    private static void testGetFlatList() throws NoSuchFieldException, IllegalAccessException {
        List<Shop> shops = new ArrayList<>();
        Shop shop = new Shop();
        List<Product> products = new ArrayList<>();
        Product p = new Product();
        p.setProductId("1");
        p.setShopId("1");
        products.add(p);
        p = new Product();
        p.setProductId("2");
        p.setShopId("2");
        products.add(p);
        shop.setProducts(products);
        shops.add(shop);
        shop = new Shop();
        products = new ArrayList<>();
        p = new Product();
        p.setProductId("3");
        p.setShopId("3");
        products.add(p);
        p = new Product();
        p.setProductId("4");
        p.setShopId("4");
        products.add(p);
        shop.setProducts(products);
        shops.add(shop);
        products = CollectUtils.getFlatList(shops, Shop::getProducts);
        for(Product pro : products){
            System.out.println(pro.getProductId()+"----"+pro.getShopId());
        }

        for(Product pro : CollectUtils.getFlatCollection(shops, Shop::getProducts, TreeSet::new)){
            System.out.println(pro.getProductId()+"----"+pro.getShopId());
        }
    }

    private static void testSetTargetProperty() throws NoSuchFieldException, IllegalAccessException {
        List<Product> products = new ArrayList<>();
        Product p = new Product();
        p.setProductId("1");
        p.setShopId("1");
        products.add(p);
        p = new Product();
        p.setProductId("2");
        p.setShopId("2");
        products.add(p);
        CollectUtils.setTargetProperty(products,"shopId",(List<String> ids) -> queryShopByIds(ids),
                new String[]{"shopName"},new String[]{"shopTelphone","telphone"});
        for(Product pro : products){
            System.out.println(pro.getShopName()+"----"+pro.getShopTelphone());
        }

        CollectUtils.setTargetProperty(products,"shopId",(List<String> ids) -> queryShopId2Name(ids), "shopName");
        for(Product pro : products){
            System.out.println(pro.getShopName()+"----"+pro.getShopTelphone());
        }
    }


    private static void testDistinctByKey() throws InstantiationException, IllegalAccessException {
        List<Product> products = new ArrayList<>();
        Product p = new Product();
        p.setProductId("1");
        p.setShopId("1");
        products.add(p);
        p = new Product();
        p.setProductId("2");
        p.setShopId("2");
        p = new Product();
        p.setProductId("2");
        p.setShopId("2");
        products.add(p);
        p = new Product();
        p.setProductId("2");
        p.setShopId("3");
        products.add(p);
        System.out.println("------------------------------------");
        List<Product> res = CollectUtils.distinctByKey(products, Product::getProductId);
        for(Product pro : res){
            System.out.println(pro.getProductId()+"----"+pro.getShopId());
        }
        System.out.println("#######################################");
        res = CollectUtils.distinctByKey(products, pro -> pro.getProductId()+"-"+pro.getShopId());
        for(Product pro : res){
            System.out.println(pro.getProductId()+"----"+pro.getShopId());
        }
    }


    private static Map<String,String> queryShopId2Name(List<String> ids) {
        Map<String,String> m = new HashMap<>();
        m.put("1","奥迪");
        m.put("2","沃尔沃");
        return m;
    }

    private static Map<String,Shop> queryShopByIds(List<String> ids) {
        // 本质上应该是查询数据库
        List<Shop> list = new ArrayList<>();
        Shop s = new Shop();
        s.setId("1");
        s.setShopName("奥迪");
        s.setTelphone("110");
        list.add(s);
        s = new Shop();
        s.setId("2");
        s.setShopName("保时捷");
        s.setTelphone("119");
        list.add(s);
        return list.stream().collect(toMap(Shop::getId, Function.identity()));
    }


    static class Shop {
         private String id;
         private String shopName;
         private String telphone;

         private List<Product> products;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getShopName() {
            return shopName;
        }

        public void setShopName(String shopName) {
            this.shopName = shopName;
        }

        public String getTelphone() {
            return telphone;
        }

        public void setTelphone(String telphone) {
            this.telphone = telphone;
        }

        public List<Product> getProducts() {
            return products;
        }

        public void setProducts(List<Product> products) {
            this.products = products;
        }
    }
     static class Product {
         private String productId;
         private String productName;
         private String shopId;
         private String shopName;
         private String shopTelphone;

         public String getProductId() {
             return productId;
         }

         public void setProductId(String productId) {
             this.productId = productId;
         }

         public String getProductName() {
             return productName;
         }

         public void setProductName(String productName) {
             this.productName = productName;
         }

         public String getShopId() {
             return shopId;
         }

         public void setShopId(String shopId) {
             this.shopId = shopId;
         }

         public String getShopName() {
             return shopName;
         }

         public void setShopName(String shopName) {
             this.shopName = shopName;
         }

         public String getShopTelphone() {
             return shopTelphone;
         }

         public void setShopTelphone(String shopTelphone) {
             this.shopTelphone = shopTelphone;
         }
     }
}
