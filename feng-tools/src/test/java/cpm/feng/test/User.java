package cpm.feng.test;

import java.math.BigDecimal;

public class User {

    private String name;
    private int age;
    private BigDecimal money;
    private double height;
    private long ll;
    public User(String name, int age){
        this.name = name;
        this.age = age;
    }

    public User(String name, int age, BigDecimal money, double height, long ll) {
        this.name = name;
        this.age = age;
        this.money = money;
        this.height = height;
        this.ll = ll;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public long getLl() {
        return ll;
    }

    public void setLl(long ll) {
        this.ll = ll;
    }
}
